namespace BlazorProject.Models;

public class Employee
{
    public string Name { get; set; }
    public string Gender { get; set; }
    public string City { get; set; }
    public int Salary { get; set; }
    public DateTime JoiningDate { get; set; }
    
}