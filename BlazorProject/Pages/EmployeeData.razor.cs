using BlazorProject.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;
using Newtonsoft.Json;

namespace BlazorProject.Pages;

public partial class EmployeeData
{
    Employee employee = new Employee();

    List<Employee> lstEmployees = new List<Employee>();

    string title;

    string companyName = "Phrase";

    string[] TableHeader = { "Name", "Gender", "City", "Salary", "Joining Date" };

    protected override async Task OnInitializedAsync()
    {
        setTitle();
        var empGetJS = (IJSInProcessRuntime)JsRuntime;
        var empList = await empGetJS.InvokeAsync<string>("employeeData.get");
        FetchEmployeeFromLocalStorage(empList);
    }

    void SaveEmployeeToLocalStorage()
    {
        employee.JoiningDate = DateTime.Now;
        lstEmployees.Add(employee);
        var empSetJS = (IJSInProcessRuntime)JsRuntime;
        empSetJS.InvokeVoid("employeeData.set", JsonConvert.SerializeObject(lstEmployees));
        employee = new Employee();
    }

    void FetchEmployeeFromLocalStorage(string empList)
    {
        if (empList != null)
        {
            lstEmployees = JsonConvert.DeserializeObject<List<Employee>>(empList);
        }
    }

    void setTitle()
    {
        string localizedTitle = Localize["EmployeeData"];
        title = string.Format(localizedTitle, companyName);
    }
}