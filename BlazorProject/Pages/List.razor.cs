using System.Net.Http.Json;
using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using BlazorProject.Modals;
using BlazorProject.Models;
using BlazorProject.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace BlazorProject.Pages;

public partial class List
{
    private List<Item> items;

    private int totalItem;

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ILocalStorageService LocalStorage { get; set; }
    
    [Inject]
    public IWebAssemblyHostEnvironment WebHostEnvironment { get; set; }

    [Inject]
    public NavigationManager NavigationManager { get; set; }
    
    [CascadingParameter] 
    public IModalService Modal { get; set; }
    
    [Inject]
    public IDataService DataService { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        // Do not treat this action if is not the first render
        if (!firstRender)
        {
            return;
        }

        var currentData = await LocalStorage.GetItemAsync<Item[]>("data");

        // Check if data exist in the local storage
        if (currentData == null)
        {
            // this code add in the local storage the fake data (we load the data sync for initialize the data before load the OnReadData method)
            var originalData = await Http.GetFromJsonAsync<Item[]>($"{NavigationManager.BaseUri}sample-data/fake-data.json");
            await LocalStorage.SetItemAsync("data", originalData);
        }
    }

    private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
    {
        if (e.CancellationToken.IsCancellationRequested)
        {
            return;
        }

        if (!e.CancellationToken.IsCancellationRequested)
        {
            items = await DataService.List(e.Page, e.PageSize);
            totalItem = await DataService.Count();
        }
    }
    
    private async void OnDelete(int id)
    {
        var parameters = new ModalParameters();
        parameters.Add(nameof(Item.Id), id);

        var modal = Modal.Show<DeleteConfirmation>("Delete Confirmation", parameters);
        var result = await modal.Result;

        if (result.Cancelled)
        {
            return;
        }

        await DataService.Delete(id);

        // Reload the page
        NavigationManager.NavigateTo("list", true);
    }
}