using System.Globalization;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace BlazorProject.Pages;

public partial class LanguageSelector
{
    private CultureInfo[] supportedLanguages = new []
    {
        new CultureInfo("en-US"),
        new CultureInfo("fr-FR"),
    };

    [Inject]
    private NavigationManager NavigationManager { get; set; }

    private CultureInfo Culture
    {
        get => CultureInfo.CurrentCulture;
        set
        {
            if (CultureInfo.CurrentCulture != value)
            {
                var js = (IJSInProcessRuntime)JsRuntime;
                js.InvokeVoid("appCulture.set", value.Name);
                NavigationManager.NavigateTo(NavigationManager.Uri, forceLoad: true);

            }
        }
    }
}