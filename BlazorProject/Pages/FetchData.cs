using System.Net.Http.Json;
using BlazorProject.Models;

namespace BlazorProject.Pages;

public partial class FetchData
{
    private Item[]? items;

    protected override async Task OnInitializedAsync()
    {
        items = await Http.GetFromJsonAsync<Item[]>("sample-data/fake-data.json");
    }
}