using BlazorProject.Models;

namespace BlazorProject.Layout;

public partial class MainLayout
{
    public List<Cake> Cakes { get; set; }

    protected override Task OnAfterRenderAsync(bool firstRender)
    {
        LoadCakes();
        StateHasChanged();
        return base.OnAfterRenderAsync(firstRender);
    }

    public void LoadCakes()
    {
        Cakes = new List<Cake>
        {
            // items hidden for display purpose
            new Cake
            {
                Id = 1,
                Name = "Red Velvet",
                Cost = 60
            },
            new Cake
            {
                Id = 2,
                Name = "Black Forest",
                Cost = 50
            },
        };
    }

}